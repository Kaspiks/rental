<html>

  <head>
    <title> Darbinieku pievienošana | Riteņu noma </title>
  </head>
  <?php session_start(); ?>
  
  <body>
    <!-- Navigation -->
<?php

require 'includes/config.php';
$conn = Connect();

$employee_name = $conn->real_escape_string($_POST['employee_name']);
$employee_surname = $conn->real_escape_string($_POST['employee_surname']);
$email = $conn->real_escape_string($_POST['email']);
$phone = $conn->real_escape_string($_POST['phone']);
$employee_availability = "Iznomā";


$query = "INSERT into employees(employee_name, employee_surname, email, phone,employee_availability) VALUES('" . $employee_name . "','" . $employee_surname . "','" . $email . "','" . $phone . "','" . $employee_availability ."')";
$success = $conn->query($query);

if (!$success){ ?>
    <div class="container">
	<div class="jumbotron" style="text-align: center;">
        <a href="enterdriver.php" class="btn btn-default"> Dodieties atpakaļ </a>
</div>
<?php	
}
else {
    header("location: enterdriver.php"); //Pareizības gadījumā dodas uz darbinieku pievienošanas sadaļu 
}

$conn->close();

include 'includes/footer.php';
?>
