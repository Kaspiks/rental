
<?php

require 'includes/config.php';
$conn = Connect();
session_start();

?>
 <?php include 'includes/header.php';?>



    <div class="container" style="margin-top: 65px;" >
    <div class="col-md-7" style="float: none; margin: 0 auto;">
      <div class="form-area">
        <form class="driver-form" role="form" action="enterdriver1.php" method="POST">
        <br style="clear: both">
          <h3 style="margin-bottom: 25px; text-align: center; font-size: 30px;"> Ievadiet darbinieku informāciju </h3>

          <div class="form-group">
          <label for="employee_name">Darbinieka Vārds</label>
            <input type="text" class="form-control" id="employee_name" name="employee_name"  required autofocus="">
          </div>

          <div class="form-group">
          <label for="employee_surname">Darbinieka Uzvārds</label>
            <input type="text" class="form-control" id="employee_surname" name="employee_surname"  required>
          </div>     

          <div class="form-group">
          <label for="phone">Darbinieka Telefona Numurs</label>
            <input type="text" class="form-control" id="phone" name="phone"  required>
          </div>

          <div class="form-group">
          <label for="email">Darbinieka E-pasta Adrese</label>
            <input type="text" class="form-control" id="email" name="email"  required>
          </div>

          <button type="submit" id="submit" name="submit" class="btn-add sm mid">Pievienot darbinieku</button>    
 
        </form>
      </div>
    </div>
    </div>
</div>

<?php include 'includes/footer.php';