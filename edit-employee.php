<?php  
require 'includes/config.php';
include 'includes/functions.php';
$conn = Connect();
session_start();
?>

               
<?php
 include 'includes/header.php';

        $employee_id = $_GET["id"]; // Pieprasa katra darbinieka raksta id parametru individuālas riteņu lapas izvadei
        $sql1 = "SELECT * FROM employees WHERE employee_id = '$employee_id'"; 
        $result1 = mysqli_query($conn, $sql1);

        if(mysqli_num_rows($result1)){
            while($row = mysqli_fetch_assoc($result1)){
                $employee_id = $row['employee_id'];
                $employee_name = $row["employee_name"];
                $employee_surname = $row["employee_surname"];
                $email = $row["email"];
                $phone = $row["phone"];
                $employee_availability = $row["employee_availability"];

            }
        }

        if(isset($_POST['update_employee'])) { // Pārstrādā formas datus kad forma ir apstipirnāta
        
        
            $employee_name           =  escape($_POST['employee_name']);
            $employee_surname        =  escape($_POST['employee_surname']);
            $email                   =  escape($_POST['email']);
            $phone                   =  escape($_POST['phone']);
            $employee_availability   =  escape($_POST['employee_availability']);
            



            $employee_name = mysqli_real_escape_string($conn, $employee_name);
    
            
              $query = "UPDATE employees SET "; // Atjaunošanas vaicājums
              $query .="employee_name  = '{$employee_name}', ";
              $query .="employee_surname  = '{$employee_surname}', ";
              $query .="email = '{$email}', ";
              $query .="phone = '{$phone}', ";
              $query .="employee_availability = '{$employee_availability}', ";
              $query .= "WHERE employee_id = {$employee_id} ";
            
            $update_post = mysqli_query($conn,$query);
            
            confirmQuery($update_post);
                        
        
        
}
        
        ?>
      <!-- Navigation -->
    <div class="container rental-container" style="margin-top: 65px;" >
    <div class="col-md-7" style="float: none; margin: 0 auto;">
      <div class="form-area">
        <form  class="rental-form" role="form" action="" enctype="multipart/form-data" method="POST"> <!-- Atjaunošanas post forma -->
        <br style="clear: both">
          <h3 style="margin-bottom: 25px; text-align: center; font-size: 30px;"> Please Provide Your Bike Details. </h3>

          <div class="form-group">
          <label for="employee_name">Darbinieka Vārds</label>
            <input  value="<?php echo htmlspecialchars(stripslashes($employee_name)); ?>" type="text" class="form-control" id="employee_name" name="employee_name"  required autofocus="">
          </div>

          <div class="form-group">
          <label for="employee_surname">Darbinieka Uzvārds</label>
            <input value="<?php echo $employee_surname; ?>" type="text" class="form-control" id="employee_surname" name="employee_surname"  required> <!-- required parametri piespiedu ievadei -->
          </div>     

          <div class="form-group">
          <label for="employee_availability">Darbinieka pieejamība  Iznomā/Neiznomā</label>
            <select name="employee_availability" class="form-control" id="employee_availability"  required>
              <option selected value="<?php echo $employee_availability; ?>"><?php echo $employee_availability?></option>
              <option value="Nomā">Iznomā</option>
              <option value="Neiznomā">Neiznomā</option>
            </select>
          </div>

          <div class="form-group">
          <label for="phone">Darbinieka Telefona Numurs</label>
            <input value="<?php echo $phone; ?>" type="text" class="form-control" id="phone" name="phone"  required>
          </div>

          <div class="form-group">
          <label for="email">Darbinieka E-pasta Adrese</label>
            <input value="<?php echo $email; ?>" type="text" class="form-control" id="email" name="email"  required>
          </div>
           <button type="submit" id="submit" name="update_driver" value="Atjaunināt Darbinieka informāciju" class="button primary rent"> Atjaunināt Darbinieka datus</button>    
        </form>
      </div>
    </div>
    </div>
</div>
<?php include 'includes/footer.php';