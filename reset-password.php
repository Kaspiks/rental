<?php
// Inicializē sesiju
session_start();

// Pārbauda vai lietotājs ir pieslēdzies, ja nav pārvirza uz pieslēgšanās lapu
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
 
// Iekļauj config failu
require_once 'includes/config.php';
$conn = Connect();

// Definē mainīgos un inicializē ar tukšām vērtībām
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";
 
// Pārstrādā formas datus kad forma ir apstipirnāta
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validē jauno paroli
    if(empty(trim($_POST["new_password"]))){
        $new_password_err = "Please enter the new password.";     
    } elseif(strlen(trim($_POST["new_password"])) < 6){
        $new_password_err = "Password must have atleast 6 characters.";
    } else{
        $new_password = trim($_POST["new_password"]);
    }
    
    // Validē atkārtoto paroli
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm the password.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($new_password_err) && ($new_password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
        
    // Pārbauda ievades kļūdas pirms atjauno datubāzi
    if(empty($new_password_err) && empty($confirm_password_err)){
        // Sagatavo atjaunošanas vaicājumu
        $sql = "UPDATE users SET password = ? WHERE id = ?";
        
        if($stmt = $mysqli->prepare($sql)){
            // Saistīt mainīgos lielumus sagatavotajā vaicājumā kā parametrus
            $stmt->bind_param("si", $param_password, $param_id);
            
            // Uzstāda parametrus
            $param_password = password_hash($new_password, PASSWORD_DEFAULT);
            $param_id = $_SESSION["id"];
            
            if($stmt->execute()){
                // Parole ir atjaunināta akurāti, iznīcina sesiju un pārvirza uz pieslēgšanās lapu
                session_destroy();
                header("location: login.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Aizver baicājumu
            $stmt->close();
        }
    }
    
    // Aizver savienojumu
    $mysqli->close();
}
?>
 
<?php include 'includes/header.php';?>
    <div class="wrapper">
        <h2>Reset Password</h2>
        <p>Please fill out this form to reset your password.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"> 
            <div class="form-group">
                <label>New Password</label>
                <input type="password" name="new_password" class="form-control <?php echo (!empty($new_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $new_password; ?>">
                <span class="invalid-feedback"><?php echo $new_password_err; ?></span>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-link ml-2" href="welcome.php">Cancel</a>
            </div>
        </form>
    </div>    
</body>
</html>