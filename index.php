<?php
include 'includes/config.php';
$conn = Connect();

session_start();
?>

<?php include 'includes/header.php';?>
        <!-- Banner -->
        <section id="banner">
            <div class="inner">
                <h2>Riteņu noma</h2>
                <p>Vēlviena riteņu nomas<br /> mājaslapa.</p>
                <ul class="actions special">
                    <li><a href="#" class="button primary">Par mums</a></li>
                </ul>
            </div>
            <a href="#one" class="more scrolly">Learn More</a>
        </section>

        <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
    <div class="panel">

        <div class="pricing-plan">
            <img src="icons/cruiser.png" alt="" class="pricing-img">
            <h2 class="pricing-header">Comfort / Cruiser</h2>
            <ul class="pricing-features">
                <li class="pricing-features-item">Paredzēta ikdienas atpūtas braucējam</li>
                <li class="pricing-features-item">Komforts un kontrole vienuviet</li>
            </ul>
        </div>

        <div class="pricing-plan">
            <img src="icons/bike.png" alt="" class="pricing-img">
            <h2 class="pricing-header">Hybrid / Commuter</h2>
            <ul class="pricing-features">
                <li class="pricing-features-item">Designed to go far and get there fast.</li>
                <li class="pricing-features-item">Perfects lai dotos apkārt pilsētai, tālākos braucienos</li>
            </ul>
        </div>

        <div class="pricing-plan">
            <img src="icons/smartmotion-xcity.png" alt="" class="pricing-img">
            <h2 class="pricing-header">Mountain / Off-road</h2>
            <ul class="pricing-features">
                <li class="pricing-features-item">Faster than traditional bike, with less effort</li>
                <li class="pricing-features-item">Perfect for mountains and off-road</li>
            </ul>
        </div>

    </div>
            </div>
        </section>

        <!-- Two -->
        <section id="two" class="wrapper alt style2">
            <section class="spotlight">
                <div class="image"><img src="images/pic01.jpg" alt="" /></div>
                <div class="content">
                    <h2>Electronic<br /> Comfort / Cruiser </h2>
                    <p>Paredzēta ikdienas atpūtas braucējam... Komforts un kontrole vienuviet</p>
                </div>
            </section>
            <section class="spotlight">
                <div class="image"><img src="images/pic02.jpg" alt="" /></div>
                <div class="content">
                    <h2>Electronic<br /> Hybrid / Commuter </h2>
                    <p>Paredzēts lai nokļūtu tālu - ātri... Perfects lai dotos apkārt pilsētai, tālākos braucienos</p>
                </div>
            </section>
            <section class="spotlight">
                <div class="image"><img src="images/pic03.jpg" alt="" /></div>
                <div class="content">
                    <h2>Electronic<br /> Mountain / Off-road</h2>
                    <p>Ātrāks nekā tradicionālais velosipēds, ar mazāku piepūli... Lieliski piemērots kalniem un bezceļiem</p>
                </div>
            </section>
        </section>

        <?php 

      
// Nosaka riteņu rakstu daudzumu lapā
$per_page = 20;


if(isset($_GET['page'])) {


$page = $_GET['page'];

} else {


$page = "";
}


if($page == "" || $page == 1) {

$page_1 = 0;

} else {

$page_1 = ($page * $per_page) - $per_page;

}



$post_query_count = "SELECT * FROM bikes";


$find_count = mysqli_query($conn,$post_query_count);
$count = mysqli_num_rows($find_count);

if($count < 1) {


echo "<h1 class='text-center'>Nav ievietoto Riteņu </h1>";




} else {


$count  = ceil($count /$per_page);




$query = "SELECT * FROM bikes LIMIT $page_1, $per_page";
$select_all_posts_query = mysqli_query($conn,$query);
?>
<section class="menu-content">
<?php
while($row = mysqli_fetch_assoc($select_all_posts_query)) {
$bike_id = $row['bike_id'];
$bike_name = $row["name"];
$bike_type = $row["type"];
$bike_teaser = $row["teaser"];
$bike_img = $row['bike_img'];
$bike_content = substr($row["content"],0,400);

?>



<!--  Riteņu raksti -->

            <a href="bike?id=<?php echo $bike_id ?>">
                <div class="bike-layout">


                    <img class="card-img-top" src="<?php echo $bike_img; ?>">
                    <h5><b> <?php echo $bike_name; ?> </b></h5>
                    <h6><?php echo $bike_type ?></h6>
                    <h6><?php echo $bike_teaser?></h6>


                </div> 
            </a>

<?php }  } ?>
</section>

           


        <section id="cta" class="wrapper style4">
            <div class="inner">
                <header>
                    <h2>Uzziniet vairāk par šo vietni</h2>
                    <p>Kārtējā velosipēdu nomas lapa</p>
                </header>
                <ul class="actions stacked">
                    <li><a href="#" class="button fit">Uzzināt Vairāk</a></li>
                </ul>
            </div>
        </section>

<?php include 'includes/footer.php';?>