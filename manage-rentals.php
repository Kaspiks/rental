
<?php
require 'includes/config.php';
$conn = Connect();
session_start();

if(isset($_POST['checkBoxArray'])) {
    foreach($_POST['checkBoxArray'] as $rentalId ){
        $bulk_options = $_POST['bulk_options'];
        switch($bulk_options) {
            case 'delete':
                $query = "DELETE FROM rented_bikes WHERE id= {$rentalId}  ";
                $update_to_delete_status = mysqli_query($conn,$query);
                break;
            case 'returned':
                $query = "UPDATE rentedbikes SET return_status = 'R' WHERE id= {$rentalId}";
                $update_to_available = mysqli_query($conn, $query);
                break;
        }
    }
}
include 'includes/header.php'
?>

    <form class="manage-b" action="" method='post'>
        <table class="table table-bordered table-hover">
            <div id="bulkOptionContainer">
                <select class="form-control" name="bulk_options" id="">
                <option value="">Izvēlieties opciju</option>
                <option value="delete">Dzēst</option>
                <option value="returned">Atgriezts</option>
            </select>
            </div>
            <div class="col-xs-4">
                <input type="submit" name="submit" class="btn btn-success btn-ap bg" value="Apstiprināt">
                <a class="btn-add" href="enterdriver.php">Pievienot</a>
            </div>
            <thead>
                <tr>
                    <th><input id="selectAllBoxes" type="checkbox"></th>
                    <th>Id</th>
                    <th> Riteņa nosaukums </th>
                    <th> Darbinieka Vārds </th>
                    <th> Darbinieka Uzvārds </th>
                    <th> Sāk.Dat </th>
                    <th> Atgriešanas.Dat </th>

                    <th> Rediģēt</th>
                    <th> Apskatīt</th>
                </tr>
            </thead>

            <tbody>
                <?php
        $query = "SELECT * FROM rentedbikes rb 
        LEFT JOIN bikes b ON rb.bike_id = b.bike_id
        LEFT JOIN employees e ON rb.employee_id = e.employee_id
        ORDER BY rb.id DESC ";
        $select_bikes = mysqli_query($conn,$query);
        while($row = mysqli_fetch_assoc($select_bikes )) {
            $rentalId = $row["id"];
            $bike_name = $row["name"];
            $employee_name = $row["employee_name"];
            $employee_surname = $row["employee_surname"];
            $employee_email = $row["email"];
            $employee_phone = $row["phone"];
            $booking_date = $row["booking_date"];
            $rent_start_date = $row["rent_start_date"];
            $rent_end_date = $row["rent_end_date"];
            $return_status = $row["return_status"];    
            echo "<tr>";
            ?>    

   <td class="cbx"><input id="cbx" type='checkbox' name='checkBoxArray[]' value='<?php echo $rentalId; ?>'  /><label for="cbx"></label><svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                <path d="M2 8.36364L6.23077 12L13 2"></path>
            </svg></td>
            <defs>
                <filter id="goo">
                    <fegaussianblur in="SourceGraphic" stddeviation="4" result="blur"></fegaussianblur>
                    <fecolormatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 22 -7" result="goo"></fecolormatrix>
                    <feblend in="SourceGraphic" in2="goo"></feblend>
                </filter>
            </defs>
        </svg>
                        <td><?php echo $rentalId; ?></td>
                        <td><?php echo $bike_name; ?></td>
                        <td><?php echo $employee_name; ?></td>
                        <td><?php echo $employee_surname; ?></td>
                        <td><?php echo $rent_start_date; ?></td>
                        <td><?php echo $rent_end_date; ?></td>
                        <td><?php echo $return_status; ?></td>


                        <td>
                            <a class='btn-add sm' href='rental?id=<?php echo $rentalId;?>'>Apskatīt</a>
                        </td>
            </tr>
            <?php

        }

        ?>
            </tbody>
        </table>
    </form>
    </div>

    <?php
include 'includes/footer.php';?>