-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2021 at 10:35 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rental`
--

-- --------------------------------------------------------

--
-- Table structure for table `bikes`
--

CREATE TABLE `bikes` (
  `bike_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `type` varchar(50) NOT NULL,
  `bike_img` varchar(150) DEFAULT 'NA',
  `menu_name` text DEFAULT NULL,
  `url` text DEFAULT NULL,
  `teaser` text DEFAULT NULL,
  `meta_title` text DEFAULT NULL,
  `meta_keywords` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bikes`
--

INSERT INTO `bikes` (`bike_id`, `name`, `content`, `type`, `bike_img`, `menu_name`, `url`, `teaser`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(19, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(32, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(33, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(34, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(35, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(36, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(37, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.'),
(38, 'Pedego', '<h1 class=\"section-title\" style=\"text-align: center;\">COMFORT CRUISER &ndash; ELECTRIC BIKE</h1>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is the original Pedego. It&rsquo;s been turning</p>\r\n<p style=\"text-align: center;\">heads and making people smile for ten years running.</p>\r\n</div>\r\n<p style=\"text-align: center;\"><img src=\"https://www.pedegoeurope.com/wp-content/uploads/2019/04/pedego-interceptor-27.jpg\" alt=\"\" width=\"1000\" height=\"824\" /></p>\r\n<hr />\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: left;\"><sup><img src=\"https://i.ytimg.com/vi/pHp64UOUzg8/maxresdefault.jpg\" alt=\"\" width=\"1006\" height=\"566\" /></sup></p>\r\n<h2 class=\"section-title\">MADE FOR YOU</h2>\r\n<div class=\"section-description\">\r\n<p>The first thing you notice when you ride a Comfort Cruiser is how comfortable you feel. You&rsquo;ll swear it was made just for you &ndash; because it was! The most important part of any Pedego is the person riding it, and the Comfort Cruiser is built with only one thing in mind: YOU.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2 class=\"section-title\" style=\"text-align: center;\">USER-FRIENDLY</h2>\r\n<div class=\"section-description\">\r\n<p style=\"text-align: center;\">The Comfort Cruiser is fully loaded with user-friendly features that make riding even more <em>fun</em>.</p>\r\n<p style=\"text-align: center;\">From the simple pleasure of a built-in bell, to innovative Smart Bike Technology including a bluetooth connected smartphone app;</p>\r\n<h1 style=\"text-align: center;\">we&rsquo;ve thought of everything!</h1>\r\n</div>\r\n</div>\r\n<p style=\"float: left;\">&nbsp;</p>\r\n<p style=\"float: right;\">&nbsp;</p>', 'Komforta', 'images/bikes/maxresdefault.jpg', 'Pedego-komforta', 'Pedego-komforta', 'Comfort Cruiser ir oriģinālais Pedego. Jau desmit gadus tas ir licis cilvēkiem smaidīt.\r\n', 'Pedego', 'cruiser, pedego, comfort, bike', 'The Comfort Cruiser is the original Pedego. It’s been turning  heads and making people smile for ten years running.');

-- --------------------------------------------------------

--
-- Table structure for table `employeebikes`
--

CREATE TABLE `employeebikes` (
  `id` int(11) NOT NULL,
  `employee_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employeebikes`
--

INSERT INTO `employeebikes` (`id`, `employee_name`) VALUES
(0, ''),
(3, ''),
(13, ''),
(14, ''),
(15, ''),
(16, ''),
(17, ''),
(1, 'kaspars20'),
(12, 'kaspars20'),
(18, 'kaspars20');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employee_id` int(11) NOT NULL,
  `employee_name` varchar(50) NOT NULL,
  `employee_surname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(8) NOT NULL,
  `employee_availability` varchar(10) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `employee_name`, `employee_surname`, `email`, `phone`, `employee_availability`) VALUES
(5, 'Emiliano', 'Emalio', 'kminajevs@gmail.com', 25167337, 'Iznomā'),
(6, 'Edvardo', 'Siriano', 'kkk', 123123123, 'Iznomā'),
(7, 'sss', 'Kekelo', 'asdasda', 0, 'Iznomā'),
(8, 'Alex', 'Alexandrio', 'kminajevs@gmail.com', 25167337, 'Iznomā');

-- --------------------------------------------------------

--
-- Table structure for table `rentedbikes`
--

CREATE TABLE `rentedbikes` (
  `id` int(100) NOT NULL,
  `bike_id` int(20) NOT NULL,
  `employee_id` int(20) NOT NULL,
  `booking_date` date NOT NULL,
  `rent_start_date` date NOT NULL,
  `rent_end_date` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `no_of_days` int(50) DEFAULT NULL,
  `return_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rentedbikes`
--

INSERT INTO `rentedbikes` (`id`, `bike_id`, `employee_id`, `booking_date`, `rent_start_date`, `rent_end_date`, `return_date`, `no_of_days`, `return_status`) VALUES
(1, 19, 5, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, 'NR'),
(2, 19, 5, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, 'NR'),
(3, 19, 7, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, 'NR'),
(4, 19, 6, '2021-07-09', '2021-07-09', '2021-07-16', NULL, NULL, 'NR'),
(5, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'NR'),
(6, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'NR'),
(7, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'NR'),
(8, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'NR'),
(9, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'NR'),
(10, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'NR'),
(11, 37, 8, '2021-07-12', '2021-07-14', '2021-07-30', NULL, NULL, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'kaspiks', '$2y$10$zWbuJt6hLr08ATe4SF19..N.2Q/JmLmcZt7gqS50yYQSk4dw29hHK', '2021-07-02 12:19:17'),
(2, 'kaspars', '$2y$10$I7XDOPrff7JS.CSsdEicC.54wwXD7DMJWhYr6oTtS/h2tH9.9YdBW', '2021-07-02 12:19:58'),
(4, 'kaspars20', '$2y$10$vgkCg/c0fqi/5ZNmPqIq6uOM5O095vaLrEQGWnE3BdfmQD6h7oU2a', '2021-07-04 12:47:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bikes`
--
ALTER TABLE `bikes`
  ADD PRIMARY KEY (`bike_id`);

--
-- Indexes for table `employeebikes`
--
ALTER TABLE `employeebikes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_username` (`employee_name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `rentedbikes`
--
ALTER TABLE `rentedbikes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bike_id` (`bike_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bikes`
--
ALTER TABLE `bikes`
  MODIFY `bike_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `rentedbikes`
--
ALTER TABLE `rentedbikes`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rentedbikes`
--
ALTER TABLE `rentedbikes`
  ADD CONSTRAINT `rentedbikes_ibfk_1` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`),
  ADD CONSTRAINT `rentedbikes_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
