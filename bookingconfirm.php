<!DOCTYPE html>
<html>

<?php 
session_start();

require 'includes/config.php';
$conn = Connect();

include 'includes/header.php';
?>



<?php


    $employee_id = $_POST['employee_id_from_dropdown'];
    $bike_id = $conn->real_escape_string($_POST['hidden_bikeid']); // Slēptais ievades lauks no bike.php 
    $rent_start_date = date('Y-m-d', strtotime($_POST['rent_start_date']));
    $rent_end_date = date('Y-m-d', strtotime($_POST['rent_end_date']));
    $return_status = "NR"; // not returned


    function dateDiff($start, $end) { //Funkcija lai iegūtu nomas sākuma un beigu datuma starpību
        $start_ts = strtotime($start);
        $end_ts = strtotime($end);
        $diff = $end_ts - $start_ts;
        return round($diff / 86400);
    }
    
    $err_date = dateDiff("$rent_start_date", "$rent_end_date");

    if($err_date >= 0) {  
    $sql1 = "INSERT into rentedbikes(bike_id,employee_id,booking_date,rent_start_date,rent_end_date,return_status) -- Ievades datu bāzes vaicājums -->
    VALUES('" . $bike_id . "','" . $employee_id . "','" . date("Y-m-d") ."','" . $rent_start_date ."','" . $rent_end_date . "','" . $return_status . "')";
    $result1 = $conn->query($sql1);

    $sql2 = "UPDATE employees SET employee_availability = 'Iznomā' WHERE employee_id = '$employee_id'"; //Atjaunošanas vaicājums darbinieku pieejamībai (Iznomā/Neiznomā)
    $result3 = $conn->query($sql2);

    $sql3 = "SELECT * FROM  bikes b, employees e, rentedbikes rb WHERE b.bike_id = '$bike_id' AND e.employee_id = '$employee_id'"; //Izvades vaicājums
    $result4 = $conn->query($sql3);


    if (mysqli_num_rows($result4) > 0) {
        while($row = mysqli_fetch_assoc($result4)) {
            $id = $row["id"];
            $bike_name = $row["name"];
            $employee_name = $row["employee_name"];
            $employee_surname = $row["employee_surname"];
            $employee_email = $row["email"];
            $employee_phone = $row["phone"];
        }
    }

    if (!$result1 | !$result3){ //Ja vaicājumi 1 un 2 ir rnepareizi izvada kļūdas ziņojumu
        die("Couldnt enter data: ".$conn->error);
    }

?>
    <div class="container">
        <div class="jumbotron">
            <h1 class="text-center" style="color: green;"><span class="glyphicon glyphicon-ok-circle"></span>Riteņa noma apstipirnāta</h1>
        </div>
    </div>
    <br>
 

    <h3 class="text-center"> <strong>Nomas numurs:</strong> <span style="color: blue;"><?php echo "$id"; ?></span> </h3>


    <div class="container"> <!-- Nomas dati -->
        <div class="box">
            <div class="col-md-10" style="float: none; margin: 0 auto; text-align: center;">
                <br>
                <h3 style="color: orange;">Ziņa</h3>
                <br>
            </div>
            <div class="col-md-10" style="float: none; margin: 0 auto; ">
                <h4> <strong>Riteņa Nosaukums: </strong> <?php echo $bike_name; ?></h4>
                <br>
                <h4> <strong>Īres Datums: </strong> <?php echo date("Y-m-d"); ?> </h4>
                <br>
                <h4> <strong>Nomas Sākuma Datums: </strong> <?php echo $rent_start_date; ?></h4>
                <br>
                <h4> <strong>Atgriešanas Datums: </strong> <?php echo $rent_end_date; ?></h4>
                <br>
                <h4> <strong>Darbinieka Vārds: </strong> <?php echo $employee_name; ?> </h4>
                <br>
                <h4> <strong>Darbinieka Uzvārds: </strong> <?php echo $employee_surname; ?> </h4>
                <br>
                <h4> <strong>Darbinieka Saziņas E-pasts:</strong>  <?php echo $employee_email; ?></h4>
                <br>
                <h4> <strong>Darbinieka Saziņas Tālrunis:</strong> +371 <?php echo  $employee_phone; ?></h4>
                <br>
            </div>
        </div>
    </div>
    <?php }
include 'includes/footer.php';